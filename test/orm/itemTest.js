var productsCategories = require("../../src/db/productsByCategories")
var productsListDB  =require("../../src/db/productsListDB");

async function getProducts() {
    await productsCategories.getProducts().then((products) => {
        for (var i = 0; i < products.length; ++i) {
            console.log(products[i].PRODUCTS_id);
            console.log(products[i].PRODUCTS_title);
            for (var j = 0; j < products[i].PRODUCTS_products.length; ++j) {
                console.log(products[i].PRODUCTS_products[j]);
            }
        }
    })
}

async function getProductsCategories() {
    await productsCategories.getCategories().then((productsCategories) => {
        // console.log(productsCategories);
        for (var i = 0; i < productsCategories.length; ++i) {
            console.log(productsCategories[i].PRODUCTS_id);
            console.log(productsCategories[i].PRODUCTS_title);
        }
    })
}

async function getProductsByCategory(idCat) {
    await productsCategories.getProductsByCategory(idCat).then((productsCategory) => {
        for (var i = 0; i < productsCategory.length; ++i) {
            console.log(productsCategory[i]);
        }
    })
}

async function getProduct(idCat, idProd) {
    let product = await productsCategories.getProduct(idCat, idProd);
    console.log(product);
}

async function addProductToList(idProd, idHousehold, quantity, remark){
    await productsListDB.insertProductToList(idProd, idHousehold, quantity, remark);
}

async function getProductsListByHousehold(idHousehold){
    let productsList = await productsListDB.getProductsListByHousehold(idHousehold);
    console.log(productsList);
}

async function removeProductList(idProd, idHousehold, quantity, remark){
    let res = await productsListDB.removeProductList(idProd, idHousehold, quantity, remark);
    console.log(res);
}

module.exports = {
    getProducts,
    getProductsCategories,
    getProductsByCategory,
    getProduct,
    addProductToList,
    getProductsListByHousehold,
    removeProductList
};

const householdDB = require('../../src/db/householdDB')
const userDB = require('../../src/db/userDB')
const crypto = require('crypto')

async function insertUser() {
    let bool = await userDB.insertUser('test', 'test', 'test@test.fr', crypto.createHash('sha256').update('test').digest("hex"), 0,);
    if (bool) {
        let user = await userDB.getUserByMail('test@test.fr')
        if (user != null) {
            householdDB.insertHousehold("test", user.USER_id).then((bool) => {
                if (bool)
                    console.log("Household added");
                else {
                    console.log("Household not added");
                }
            })
        }
        else {
            console.log("User not added")
        }
    }

    // await householdDB.insertHousehold("test", 2).then((bool) => {
    //     if (bool)
    //         console.log("Household added");
    //     else {
    //         console.log("Household not added");
    //     }
    // })


    await userDB.getUserByMail('test@test.fr').then((user) => {
        if (user != null) console.log(user);
        else console.log("User not found");
    })

    await userDB.getUserById(1).then((user) => {
        if (user != null) console.log(user);
        else console.log("User not found");
    })

    await userDB.getUsers().then((users) => {
        if (users != null) {
            for (var i = 0; i < users.length; ++i) {
                console.log(users[i])
            }
        }
        else console.log("Users not found");
    })
}

async function updateUser() {
    await userDB.setMail(1, 'test2@test2.fr').then((res) => {
        if (res.ok === 1)
            console.log("Mail updated");
        else
            console.log("Mail not updated");
    })

    await userDB.setName(1, 'test2').then((res) => {
        if (res.ok === 1)
            console.log("Name updated");
        else
            console.log("Name not updated");
    })

    await userDB.setFirstname(1, 'test2').then((res) => {
        if (res.ok === 1)
            console.log("Firstname updated");
        else
            console.log("Firstname not updated");
    })


    await userDB.setState(1, 1).then((res) => {
        if (res.ok === 1)
            console.log("State updated");
        else
            console.log("State not updated");
    })


    userDB.setPassword(1, crypto.createHash('sha256').update('test2').digest("hex")).then((res) => {
        if (res.ok === 1)
            console.log("Pwd updated");
        else
            console.log("Pwd not updated");
    })
}

module.exports = {insertUser, updateUser};

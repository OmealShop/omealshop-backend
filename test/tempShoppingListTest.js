//process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);


describe('Create and get the temporary shopping list of the current user ', () => {
    it('it should insert a product in the data base', (done) => {
        chai.request(server)
            .post('/user/1/shoppingList')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

});

describe('get the temporary shopping list of the current user ', () => {
    it('it should insert a product in the data base', (done) => {
        chai.request(server)
            .get('/user/1/shoppingList')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

});

describe('delete the temporary shopping list of the current user ', () => {
    it('it should insert a product in the data base', (done) => {
        chai.request(server)
            .delete('/user/1/shoppingList')
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

});
//process.env.NODE_ENV = 'test';
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

/*
* Test the //user/:id/productsList route
* Get an existing list
*/

describe('get The products list of a household from the database', () => {
    it('it should get the products list of a household from data base', (done) => {
        chai.request(server)
            .get('/user/1/productsList')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            });
    });

});

/*
* Test the /user/:id/productsList/add route
*/

describe('Add product to the shopping list of the current user', () => {
    it('it should insert a product in the data base', (done) => {
        let product = {
            idProd: 11,
            quantity: 0 ,
            remark: "nothing",
        }
        chai.request(server)
            .post('/user/1/productsList/add ')
            .send(product)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                res.body.product.should.have.property('PRODUCTS_LIST_id_prod');
                res.body.product.should.have.property('PRODUCTS_LIST_id_household');
                res.body.product.should.have.property('PRODUCTS_LIST_quantity');
                res.body.product.should.have.property('PRODUCTS_LIST_remark')
                done();
            });
    });

});

/*
* Test the /user/:id/productsList/:idProd route
*/

describe('Remove product from the shopping list', () => {
    it('it should delete a product from the data base', (done) => {
        let product = {
            quantity: 5 ,
            remark: "nothing",
        }
        chai.request(server)
            .delete('/user/1/productsList/11')
            .send(product)
            .end((err, res) => {
                res.should.have.status(200);
                done();
            });
    });

});

/*
* Test the /user/:id/productsList/:idProd route
*/
describe('Edit a product of the shopping list', () => {
    it('it should update the product with the right quantity and the right comment', (done) => {
        let product = {
            quantity: 5,
            remark: "Update product",
        }
        chai.request(server)
            .patch('/user/1/productsList/11')
            .send(product)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('object');
                done();
            });
    })
});


//process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

chai.use(chaiHttp);

 
  /*
  * Test the /login route
  * Correct password
  */
  describe('Login', () => {
      it('it should get a user and check his email and password and return a success code 200', (done) => {
          let user = {
            email: "test@test.fr",
            pwd: "test",    
          }
        chai.request(server)
            .post('/login')
            .send(user)
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('object');
                  res.body.user.should.have.property('USER_mail');
                  res.body.user.should.have.property('USER_pwd');
done();
});
});

});


/*
* Test the /login route
* Incorrect password
*/
describe('Login', () => {
    it('it try to log in with a wrong password and return an error code 403', (done) => {
        let user = {
            email: "test@test.fr",
            pwd: "testetst",
        }
        chai.request(server)
            .post('/login')
            .send(user)
            .end((err, res) => {
                res.should.have.status(403);
                done();
            });
    });

});


/*
* Test the /login route
* User doesn't exist
*/
describe('Login', () => {
    it('it should try to log in with unregistred user and return an error code 500', (done) => {
        let user = {
            email: "toto@toto.fr",
            pwd: "toto",
        }
        chai.request(server)
            .post('/login')
            .send(user)
            .end((err, res) => {
                res.should.have.status(500);
                done();
            });
    });

});
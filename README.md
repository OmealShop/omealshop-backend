![alt](images/logo-black.png)

# OMealShop

This is the back-end code of OMealShop

## Description

OMealShop is a website that enable to create and manage easily a shopping list for home.

It's possible to  :

- Plan meal for weeks
- Add products to the home's shopping list
- Add several members of a family to the home
- Create meal or manage those who exists

## Deployment
This is the instructions to deploy the front end code :
- git clone https://gitlab.com/OmealShop/omealshop-backend
- npm install
- npm start

The website is accessible at localhost:3000

## Environnement
OMealShop-backend is developped with :
- NodeJS express
- GraphQL API
- Mongooose
- Docker (soon)

## RoadMap

The roadmap of the project can be viewed [here](https://tree.taiga.io/project/alexispoyen-omealshop/backlog)

List of things to do :

- [ ] Creation of account
- [ ] Creation of household groups
- [ ] Manage household groups
- [ ] Add products to shopping list
- [ ] Manage meal
- [ ] Plan meal
- [ ] Generate shopping list

## Licence
OMealShop is under GNU GPLv3 licence
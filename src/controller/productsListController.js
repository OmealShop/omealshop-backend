
const productListDB = require("../db/productsListDB")

exports.insert_product = async function addProductToList(idProd, idHousehold, quantity, remark, res) {
    console.log(quantity);
    console.log(remark);
    await productListDB.insertProductToList(idProd,idHousehold,quantity,remark);
        let product = await productListDB.getProduct(idProd,idHousehold,quantity,remark);
        console.log(product);
        if(product == null){
            res.status(500).json({error: "Error while insertion"});
        } else {
            res.status(200).json({product: product});
        }
};
exports.products_shopping_list = async function getProductsOfTheList(idHousehold,res){
    let list = await productListDB.getProductsListByHousehold(idHousehold);
    //console.log(list);
    res.status(200).json({products: list});

};
exports.remove_product = async function removeProductFromList(res,idProd,idHousehold){

    await productListDB.removeProductList(idProd, idHousehold) ;
    res.status(200);
    res.send();


};

exports.edit_product = async function  editProduct(idProd, idHousehold, quantity, remark, res)
{
     await productListDB.editProduct(idProd,idHousehold,quantity,remark);
     let product = await productListDB.getProduct(idProd,idHousehold,quantity,remark,res);

     if(product != ''){
         res.status(200).json({product : product});
     } else {
         res.status(500).json({error : "Error while updating"});
     }

}
exports.crud_product = async function crudProduct(idProd, idHousehold, quantity, remark,res){

    let product = await productListDB.getProduct(idProd,idHousehold);
    console.log(product);
    if(product != null){
       if(quantity == 0) {
           console.log("remove");
           this.remove_product(product.PRODUCTS_LIST_id_prod,product.PRODUCTS_LIST_id_household,product.PRODUCTS_LIST_quantity,product.PRODUCTS_LIST_remark,res);

       } else if (quantity > 0 ){
           console.log("edit")
           this.edit_product(idProd,idHousehold,quantity,remark,res);
       }
   } else if(product == null) {
        if(quantity > 0){
            console.log("Add")
            this.insert_product(idProd,idHousehold,quantity,remark,res);
        } else {
            console.log("nada")
            res.status(500).json({error : "Error"});
        }

   }

}
var mongoose = require('mongoose');
const userDB = require('../db/userDB.js')
const householdDB = require('../db/householdDB');


const crypto = require('crypto');

exports.index = function(req, res) {
  
};

exports.all_users = function(callback){
     userDB.getUsers((err, users) =>{
        if(err){
            console.log(err);
        }
        else{
            console.log(users);
            callback(users);
        }
    });
  
};

exports.user_login = async function(email, pwd,res) {
   let pwd_hashed = crypto.createHash('sha256').update(pwd).digest("hex");
   await userDB.getUserByMail(email).then(async (user) => {
    //console.log( user);
    if(user == null){
        res.status(500).json({error: "You do not have rights to visit this page"});
   } else{
        if(pwd_hashed == user.USER_pwd){
            let households =[];
            for (let i = 0; i < user.USER_households.length; ++i) {
                await householdDB.getHouseholdById(user.USER_households[i]).then((household) => {
                    households.push(household);
                });
            }
            res.status(200).json({user: user, households: households});
        } else {
            res.status(403).json({error: "Wrong password/Email"});

        }
   }
})
     
};


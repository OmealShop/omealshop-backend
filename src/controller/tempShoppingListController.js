const productsListDB = require("../db/productsListDB");

const tempShoppingListDB = require("../../src/db/tempShoppingListDB")

exports.createTempShoppingList = async function(req, res,idHouseHold){
    let bool = await tempShoppingListDB.createTempShoppingList(idHouseHold);
    if(bool){
        let productsList = await productsListDB.getProductsListByHousehold(idHouseHold);
        for (let i = 0; i < productsList.length; ++i) {

            await tempShoppingListDB.insertProductIntempShoppingList(idHouseHold,productsList[i]);
        }
        let tempShoppingList = await tempShoppingListDB.getTempShoppingList(idHouseHold);
        res.status(200).json({tempShoppingList: tempShoppingList});
    }
};

exports.getShoppingList = async function(req,res,idHouseHold){
    let shoppingList = await tempShoppingListDB.getTempShoppingList(idHouseHold);
    res.status(200).json({shoppingList : shoppingList});
}

exports.deleteShoppingList = async function (req,res,idHouseHold) {
    await tempShoppingListDB.deleteShoppingList(idHouseHold);
    res.status(200);
    res.send();
}

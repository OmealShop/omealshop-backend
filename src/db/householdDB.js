var mongoose = require('mongoose');
var config = require('../../config');

mongoose.connect(config.DB_HOST_URL, function (error) {
    if (error) {
        throw err;
    }
});

var householdSchema = new mongoose.Schema({
        HOUSEHOLD_id: {type: Number},
        HOUSEHOLD_name: {type: String, match: /^[a-zA-Z0-9-_]+$/},
        HOUSEHOLD_user_creator: Number,
        HOUSEHOLD_members: [Number],
        HOUSEHOLD_creation_date: {type: Date, default: Date.now}
    },
    {versionKey: false}
    )
;

module.exports.householdSchema = householdSchema;

var householdModel = mongoose.model('household', householdSchema, 'household');

module.exports.householdModel = householdModel;

async function insertHousehold(name, creator) {
    await getNewId().then((household) => {
        var id;
        if (household != null)
            id = household.HOUSEHOLD_id + 1;
        else
            id = 1;

        var newHousehold = new householdModel({
            HOUSEHOLD_id: id,
            HOUSEHOLD_name: name,
            HOUSEHOLD_user_creator: creator
        })

        var promise = newHousehold.save();

        promise.then(function (household) {
            addMember(household.HOUSEHOLD_id, household.HOUSEHOLD_user_creator).then((household) => {
                if(household.ok === 1)
                    console.log("Member added");
                else
                    console.log("Member not added");
            })
        })
    })
    return true;
}

async function addMember(foyerId, userId) {
    return householdModel.updateOne({HOUSEHOLD_id: foyerId}, {$push: {HOUSEHOLD_members: [userId]}});
}

function getNewId() {
    return householdModel.findOne({})
        .sort("-HOUSEHOLD_id").limit(1).exec();

}
function getAllHousehold(){
    return householdModel.find()
}
function getHouseholdById(foyerId){
    return householdModel.findOne({
        HOUSEHOLD_id: foyerId
    });
}


module.exports = {insertHousehold, getAllHousehold, getHouseholdById};
const mongoose = require('mongoose');
const config = require('../../config');

mongoose.connect(config.DB_HOST_URL, function (error) {
    if (error) {
        throw error;
    }
});

var productsListSchema = new mongoose.Schema({
        PRODUCTS_LIST_id_prod: {type: Number},
        PRODUCTS_LIST_id_household: {type: Number},
        PRODUCTS_LIST_quantity: {type: Number},
        PRODUCTS_LIST_remark: {type: String}
    },
    {versionKey: false}
    )
;

var productsListModel = mongoose.model('productsListModel', productsListSchema, 'products_list');

async function insertProductToList(idProduct, idHousehold, quantity, remark) {

    let newProductToList = new productsListModel({
        PRODUCTS_LIST_id_prod : idProduct,
        PRODUCTS_LIST_id_household: idHousehold,
        PRODUCTS_LIST_quantity: quantity,
        PRODUCTS_LIST_remark: remark
    });

    await newProductToList.save();
    return true;
}

async function getProductsListByHousehold(idHousehold){
    return productsListModel.find({PRODUCTS_LIST_id_household: idHousehold});
}
async function getProduct(idProduct,idHouseHold, quantity, remark){
    return productsListModel.findOne({
        PRODUCTS_LIST_id_prod : idProduct,
        PRODUCTS_LIST_id_household : idHouseHold,
        PRODUCTS_LIST_quantity: quantity,
        PRODUCTS_LIST_remark: remark,

    });
}
async function getProduct(idProduct, idHouseHold){
    return productsListModel.findOne({
        PRODUCTS_LIST_id_prod : idProduct,
        PRODUCTS_LIST_id_household : idHouseHold,

    });
}
async function editProduct(idProduct, idHouseHold, quantity, remark ){
   return productsListModel.updateOne({
       PRODUCTS_LIST_id_prod : idProduct,
       PRODUCTS_LIST_id_household : idHouseHold,
   }, {
       PRODUCTS_LIST_quantity: quantity,
       PRODUCTS_LIST_remark: remark,
   });
}

async function removeProductList(idProd, idHousehold){
    return productsListModel.deleteOne({PRODUCTS_LIST_id_prod: idProd, PRODUCTS_LIST_id_household: idHousehold});

}

module.exports = {insertProductToList, getProductsListByHousehold, getProduct,removeProductList, editProduct}

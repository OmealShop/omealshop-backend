const mongoose = require('mongoose');
const config = require('../../config');
const productsDB = require('./productsDB');

mongoose.connect(config.DB_HOST_URL, function (error) {
    if (error) {
        throw error;
    }
});

var productsSchema = new mongoose.Schema({
        PRODUCTS_id: {type: Number},
        PRODUCTS_title: {type: String},
        PRODUCTS_products: [productsDB.productSchema]
    },
    {versionKey: false}
    )
;

var productsModel = mongoose.model('productsModel', productsSchema, 'products');

async function insertCategory(id, title) {

    var newProduct = new productsModel({
        PRODUCTS_id: id,
        PRODUCTS_title: title
    })

    await newProduct.save();
    return true;
}

async function addProduct(productID, product) {
    return productsModel.updateOne({PRODUCTS_id: productID}, {$push: {PRODUCTS_products: product}});
}

async function getProducts() {
    return productsModel.find();
}

async function getCategories() {
    return productsModel.find({}, 'PRODUCTS_id PRODUCTS_title');
}

async function getCategory(idCat) {
    return productsModel.findOne({PRODUCTS_id: idCat});
}

async function getProduct(idCat, idProd) {
    let category = await productsModel.findOne({PRODUCTS_id: idCat});
    for (var i = 0; i < category.PRODUCTS_products.length; ++i) {
        if (category.PRODUCTS_products[i].PRODUCT_id == idProd) {
            return category.PRODUCTS_products[i];
        }
    }
}

async function setIsFood(idCat , idProd, isFood) {
    let product  = await getProduct(idCat, idProd);
    product.PRODUCT_isFood = isFood;
    await productsModel.updateOne({PRODUCTS_id: idCat}, {$pull: {PRODUCTS_products: {PRODUCT_id: idProd}}})
    return productsModel.updateOne({PRODUCTS_id: idCat}, {$push: {PRODUCTS_products: product}})
}

module.exports = {getProducts, getCategories, getProductsByCategory: getCategory, getProduct, setIsFood};

var mongoose = require('mongoose');
var config = require('../../config');

mongoose.connect(config.DB_HOST_URL, function (error) {
    if (error) {
        throw error;
    }
});

var productSchema = new mongoose.Schema({
        PRODUCT_id: {type: Number},
        PRODUCT_name: {type: String},
        PRODUCT_unit: {type: String},
        PRODUCT_category: {type: Number},
        PRODUCT_isFood: {type: Boolean}
    },
    {versionKey: false}
    )
;


var productsModel = mongoose.model('products', productSchema);

async function insertProducts(id, name, unit, idCategory, isFood) {
    return new productsModel({
        PRODUCT_id: id,
        PRODUCT_name: name,
        PRODUCT_unit: unit,
        PRODUCT_category: idCategory,
        PRODUCT_isFood: isFood
    });
}


module.exports = {insertProducts, productSchema};


const mongoose = require('mongoose');
const config = require('../../config');
const productsDB = require('./productsDB');
const productsListDB = require('./productsListDB')

mongoose.connect(config.DB_HOST_URL, function (error) {
    if (error) {
        throw err;
    }
});

let tempShoppingListSchema = new mongoose.Schema({
        TEMPSHOPPINGLIST_id: {type: Number},
        TEMPSHOPPINGLIST_houseHold_id : {type: Number},
        TEMPSHOPPINGLIST_creation_date: {type: Date, default: Date.now},
        TEMPSHOPPINGLIST_products: []
    },
    {versionKey: false}
    )
;

module.exports.tempShoppingListSchema = tempShoppingListSchema;

let tempShoppingListModel = mongoose.model('tempShoppingListModel', tempShoppingListSchema, 'temp_shopping_list');

module.exports.tempShoppingListModel = tempShoppingListModel;

async function createTempShoppingList(householdId){


    let newTempShoppingList = new tempShoppingListModel({
        TEMPSHOPPINGLIST_houseHold_id: householdId,
        TEMPSHOPPINGLIST_products: [],
    });


    await newTempShoppingList.save();

    return true;

}

async function insertProductIntempShoppingList(householdId,product){
    return tempShoppingListModel.update({TEMPSHOPPINGLIST_houseHold_id: householdId}, {$push:
            {TEMPSHOPPINGLIST_products:
                {   PRODUCTS_LIST_id_prod : product.PRODUCTS_LIST_id_prod,
                    PRODUCTS_LIST_id_household : householdId,
                    PRODUCTS_LIST_quantity : product.PRODUCTS_LIST_quantity,
                    PRODUCTS_LIST_remark : product.PRODUCTS_LIST_remark,
                 }
            }
    });

}

async function getTempShoppingList(householdId){

    return tempShoppingListModel.findOne({
        TEMPSHOPPINGLIST_houseHold_id : householdId
    });
}

async function deleteShoppingList(householdId){
    return tempShoppingListModel.deleteOne({
        TEMPSHOPPINGLIST_houseHold_id : householdId
    });
}

module.exports = {deleteShoppingList,getTempShoppingList,insertProductIntempShoppingList,createTempShoppingList};
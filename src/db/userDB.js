var mongoose = require('mongoose');
var config = require('../../config');

mongoose.connect(config.DB_HOST_URL, function (error) {
    if (error) {
        throw error;
    }
});

var userSchema = new mongoose.Schema({
        USER_id: {type: Number},
        USER_name: {type: String, match: /^[a-zA-Z0-9-_]+$/},
        USER_firstname: {type: String, match: /^[a-zA-Z0-9-_]+$/},
        USER_households: [Number],
        USER_mail: {type: String, match: /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/},
        USER_pwd: {type: String},
        USER_state: {type: Number},
        USER_creation_date: {type: Date, default: Date.now}
    },
    {versionKey: false}
    )
;


var userModel = mongoose.model('user', userSchema, 'user');


async function insertUser(name, firstname, mail, pwd, state, callback) {
    await getNewId().then(async (user) => {
        var id;
        if (user != null)
            id = user.USER_id + 1;
        else
            id = 1;

        var newUser = new userModel({
            USER_id: id,
            USER_name: name,
            USER_firstname: firstname,
            USER_mail: mail,
            USER_pwd: pwd,
            USER_state: state
        })

        await newUser.save();
    })
    return true;
}

function getUserByMail(mail) {
    return userModel.findOne({USER_mail: mail});
}


function getUserById(id) {
    return userModel.findOne({USER_id: id});
}

function getUsers() {
    return userModel.find(null);
}

function setMail(id, mail) {
    return userModel.updateOne({USER_id: id}, {USER_mail: mail})
}

function setName(id, name) {
    return userModel.updateOne({USER_id: id}, {USER_name: name})
}

function setFirstname(id, firstname) {
    return userModel.updateOne({USER_id: id}, {USER_firstname: firstname})
}

function setState(id, state) {
    return userModel.updateOne({USER_id: id}, {USER_state: state})
}

function setPassword(id, pwd) {
    return userModel.updateOne({USER_id: id}, {USER_pwd: pwd})
}

function getNewId() {
    return userModel.findOne({})
        .sort("-USER_id").limit(1).exec();
}

module.exports = {insertUser, getUserByMail, getUserById, getUsers, setMail, setName, setFirstname, setState, setPassword, userSchema, getNewId}

const express = require('express')
const app = express()
const bodyParser = require("body-parser");
const cors = require("cors");

//const userDB = require('./src/db/userDB');

const graphqlHTTP = require('express-graphql');
var {buildSchema} = require('graphql');
var schema = buildSchema(`
  type Query {
    hello: String
  }
`);

app.use(cors());

app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql: true
}));

const loginController = require('./src/controller/loginController');
const productController = require('./src/controller/productController');
const categoriesController = require('./src/controller/categoriesController');
const productsListController = require('./src/controller/productsListController');
const tempShoppingListController = require('./src/controller/tempShoppingListController');
const userTest = require('./test/orm/userTest');
const itemTest = require('./test/orm/itemTest');

/** bodyParser.urlencoded(options)
 * Parses the text as URL encoded data (which is how browsers tend to send form data from regular forms set to POST)
 * and exposes the resulting object (containing the keys and values) on req.body
 */
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

/**bodyParser.json(options)
 * Parses the text as JSON and exposes the resulting object on req.body.
 */
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.send('Hello World!')
})

app.get('/test/orm/user/userInsert', function (req, res) {
    userTest.insertUser()
    res.send('Hello World!')
})

app.get('/test/orm/user/userUpdate', function (req, res) {
    userTest.updateUser()
    res.send('Hello World!')
})

app.get('/test/orm/items/getItems', function (req, res) {
    // itemTest.getProducts();
    // itemTest.getProductsCategories();
    // itemTest.getProductsByCategory(1);
    // itemTest.getProduct(1, 1);
    itemTest.addProductToList(1,1,5,"test");
    itemTest.getProductsListByHousehold(1);
    itemTest.removeProductList(1,1,5,"test");
    res.send('Hello World!')
})

app.get('/test/orm/items/addItemShoppingList', function (req, res) {
    itemTest.addProductShoppingList();
})
app.get('/allUsers', function (req, res) {
    loginController.all_users((users) => {
        res.send(users)
    });
});

app.post('/login', function (req, res) {
    let email = req.body.email;
    let pwd = req.body.pwd;

    loginController.user_login(email, pwd, res);
});
app.post('/user/:id/productsList/add', function(req,res){
    let idHouseHold = req.params.id;
    let idProd = req.body.idProd;
    let quantity = req.body.quantity;
    let remark = req.body.remark;
    productsListController.insert_product(idProd,idHouseHold,quantity,remark,res);
});
app.get('/user/:id/productsList', function(req,res) {
    let idHouseHold = req.params.id;
    productsListController.products_shopping_list(idHouseHold,res)
});

app.delete('/user/:id/productsList/:idProd', function(req,res){
    let idProd = req.params.idProd;
    let idHouseHold = req.params.id;
    productsListController.remove_product(res,idProd,idHouseHold);
});

app.patch('/user/:id/productsList/:idProd', function(req,res){
    let idProd = req.params.idProd;
    let idHouseHold = req.params.id;
    let quantity = req.body.quantity;
    let remark = req.body.remark;
    productsListController.edit_product(idProd, idHouseHold,quantity,remark,res);
});

/*app.post('/crudProduct', function(req,res){
    let idProd = req.body.idProd;
    let idHouseHold = req.body.idHouseHold;
    let quantity = req.body.quantity;
    let remark = req.body.remark;
    productsListController.crud_product(idProd,idHouseHold,quantity,remark,res);
});*/
app.get('/products',  function(req,res){
    productController.all_products(req,res);
});
app.get('/categories', function(req,res){
    categoriesController.all_categories(req,res);
});

app.post('/user/:id/shoppingList', function(req,res){
    let idHouseHold = req.params.id;
    tempShoppingListController.createTempShoppingList(req,res,idHouseHold);
});

app.get('/user/:id/shoppingList', function(req,res){
    let idHouseHold = req.params.id;
    tempShoppingListController.getShoppingList(req,res,idHouseHold);
}) ;
app.delete('/user/:id/shoppingList', function(req,res){
    let idHouseHold = req.params.id;
    tempShoppingListController.deleteShoppingList(req,res,idHouseHold);
});



app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});
module.exports=app;